import sys
import os

arguments = sys.argv

if len(arguments) <= 1:
    exit("Missing version run as 'python deploy.py <app_id>'")

app_id = sys.argv[1]

os.system('python init.py')
os.system('gcloud app deploy app.yaml --version 5 --project ' + app_id)
