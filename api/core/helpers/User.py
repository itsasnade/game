from google.appengine.api import memcache

from api.models.UserModel import GameModel


def get_user_from_request(req):
    headers = req.headers
    if 'GAME-CODE' not in headers:
        return None

    game_code = headers['GAME-CODE']

    return get_user_from_game_code(game_code)


def get_user_from_game_code(game_code):
    user_key = memcache.get(key=game_code)
    if not user_key:
        game_model = GameModel.get_by_id(game_code)
        if not game_model:
            return None

        user = game_model.user.get()
    else:
        user = user_key.get()

    if not user:
        return None

    if user:
        user_key = user.key
        memcache.set(key=game_code, value=user_key)

    return user
