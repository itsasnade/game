ITEMS = [
    {
        "kind": "body_guard",
        "label": "Bodyguard",
        "description": "Hire a bodyguard to protect you from physical threats. This increases your guard.",
        "cost": 200,
        "influence": 0,
        "guard": 20,
        "suspicion_level_decrease": 0,
        "scale_up": False,
        "order": 1
    },
    {
        "kind": "spy",
        "label": "Corporate spy",
        "description": "Hire a spy to infiltrate other companies to gather information. This increases your influence",
        "cost": 500,
        "influence": 30,
        "guard": 0,
        "suspicion_level_decrease": 0,
        "scale_up": False,
        "order": 2
    },
    {
        "kind": "bribe_police",
        "label": "Bribe a police officer.",
        "description": "Bribe a police officer to look to other way. This increases your guard and lowers your suspect meter",
        "cost": 2000,
        "influence": 100,
        "guard": 200,
        "suspicion_level_decrease": 30,
        "scale_up": True,
        "order": 3
    },
    {
        "kind": "bribe_politician",
        "label": "Bribe a politician.",
        "description": "Bribe a politician to make things go your way.",
        "cost": 5000,
        "influence": 200,
        "guard": 100,
        "suspicion_level_decrease": 60,
        "scale_up": True,
        "order": 4
    },
    {
        "kind": "bribe_president",
        "label": "Bribe the president.",
        "description": "Bribe a president to make things go your way and have things disappear",
        "cost": 5000000,
        "influence": 50000,
        "guard": 50000,
        "suspicion_level_decrease": 100,
        "scale_up": True,
        "order": 5
    },
]
