ACTIONS = [
    {
        "kind": "break_out",
        "label": "Jail",
        "description": "The jails are running out space. Break out some criminals to gain their respect.",
        "timeout": 60,
        "base_money": 0,
        "base_influence": 1000,
        "base_guard": 1000,
        "success_rate": 20,
        "suspicion_level_increase": 50,
        "order": 1
    },
    {
        "kind": "blackmail_person",
        "label": "Blackmail",
        "description": "Blackmail a person into giving you money",
        "timeout": 10,
        "base_money": 20,
        "base_influence": 0,
        "base_guard": 0,
        "success_rate": 80,
        "suspicion_level_increase": 5,
        "order": 2
    },
    {
        "kind": "sell_weapons",
        "label": "Weapons",
        "description": "Sell some weapons to gangs",
        "timeout": 60,
        "base_money": 250,
        "base_influence": 2,
        "base_guard": 0,
        "success_rate": 70,
        "suspicion_level_increase": 20,
        "order": 3
    },
    {
        "kind": "sell_weed",
        "label": "Weed",
        "description": "Sell weed to teenagers",
        "timeout": 5,
        "base_money": 15,
        "base_influence": 0,
        "base_guard": 0,
        "success_rate": 70,
        "suspicion_level_increase": 10,
        "order": 4
    }
    ,
    {
        "kind": "sell_drugs",
        "label": "Drugs",
        "description": "Sell drugs to addicts",
        "timeout": 20,
        "base_money": 50,
        "base_influence": 0,
        "base_guard": 0,
        "success_rate": 60,
        "suspicion_level_increase": 12,
        "order": 5
    }
]