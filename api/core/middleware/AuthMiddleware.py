import falcon

from api.core.helpers.User import get_user_from_request


class AuthMiddleware(object):

    def process_request(self, req, resp):
        if req.path != '/api/game' and req.method != "OPTIONS":
            user = get_user_from_request(req)

            if not user:
                raise falcon.HTTPError(
                    falcon.HTTP_UNAUTHORIZED,
                    title="not_authenticated"
                )
