from google.appengine.ext import ndb

from api.models import UserModel


ACTIONS = [
    {
        "kind": "rob_person",
        "timeout": 10,
        "base_money": 10,
        "base_influence": 0,
        "base_guard": 0,
    },
    {
        "kind": "steal_car",
        "timeout": 60,
        "base_money": 10,
        "base_influence": 5,
        "base_guard": 2,
    }
]


class TaskModel(ndb.Model):
    kind = ndb.StringProperty(choices=["rob_person", "steal_car"])
    total = ndb.IntegerProperty()  # 5
    count = ndb.IntegerProperty(default=0)


class MissionModel(ndb.Model):
    name = ndb.StringProperty()
    description = ndb.StringProperty()
    # requiredAvg = ndb.FloatProperty(default=0.0)
    # rewardType = ndb.IntegerProperty()
    # maxReward = ndb.FloatProperty(default=0.0)
    # successRateModifier = ndb.FloatProperty(default=0.0)
    # missionCooldownTime = ndb.FloatProperty(default=0.0)

    mission_type = ndb.StringProperty(choices=[])

    reward_influence = ndb.FloatProperty(default=0.0)
    reward_guard = ndb.FloatProperty(default=0.0)
    reward_money = ndb.FloatProperty(default=0.0)

    failure_influence = ndb.FloatProperty(default=0.0)
    failure_guard = ndb.FloatProperty(default=0.0)
    failure_money = ndb.FloatProperty(default=0.0)

    complete_requirements = ndb.KeyProperty(kind=TaskModel, repeated=True)

    created_on = ndb.DateTimeProperty(auto_now_add=True)
    accepted_on = ndb.DateTimeProperty()
    completed_on = ndb.DateTimeProperty()

    for_user = ndb.KeyProperty(kind=UserModel)

