from google.appengine.ext import ndb


class TimerModel(ndb.Model):
    kind = ndb.StringProperty()
    last_action = ndb.DateTimeProperty()


class UserModel(ndb.Model):
    created = ndb.DateTimeProperty(auto_now=True)

    username = ndb.StringProperty()
    display_name = ndb.StringProperty()

    money = ndb.FloatProperty(default=1000)
    influence = ndb.FloatProperty(default=1)
    guard = ndb.FloatProperty(default=0.0)

    suspicion_level = ndb.IntegerProperty(default=0)

    last_time_actions = ndb.KeyProperty(indexed=False, repeated=True, kind=TimerModel)

    average_score = ndb.ComputedProperty(lambda self: round((self.influence + self.guard) / 2))

    def make_dict(self):
        user_dict = self.to_dict(exclude=['last_time_actions', 'created'])

        if 'urlsafe' not in user_dict:
            user_dict['urlsafe'] = self.key.urlsafe()

        return user_dict


class GameModel(ndb.Model):
    user = ndb.KeyProperty(kind=UserModel, required=True)
