import math

import falcon
import json

from api.core.helpers.User import get_user_from_request
from api.core.static.ShopList import ITEMS


class ShopResource(object):

    def on_get(self, req, resp):
        user = get_user_from_request(req)

        if not user:
            resp.body = json.dumps({
                'message': 'user_not_found',
            })
            resp.status = falcon.HTTP_400
            return

        actions_dict = {item['kind']: item for item in ITEMS}

        for action in actions_dict.values():
            actual_price = action['cost']

            if action['scale_up']:
                cost_increase_modifier = 1 + (user.influence * 0.0002)
            else:
                cost_increase_modifier = 1

            actual_price = round(actual_price * cost_increase_modifier)

            action['actual_cost'] = actual_price

        resp.body = json.dumps({
            "message": "ok",
            "items": sorted(actions_dict.values(), key=lambda k: k['order'])
        })
        resp.status = falcon.HTTP_200
        return

    def on_post(self, req, resp):
        kind = str(req.media.get('kind')).lower()
        quantity = math.floor(req.media.get('quantity', 1))

        if not kind:
            resp.body = json.dumps({
                "message": "kind_empty"
            })
            resp.status = falcon.HTTP_400
            return

        user = get_user_from_request(req)

        if not user:
            resp.body = json.dumps({
                'message': 'user_not_found',
                'player': {}
            })
            resp.status = falcon.HTTP_400
            return

        items_dict = {item['kind']: item for item in ITEMS}
        item = items_dict[kind]

        if item['scale_up']:
            cost_increase_modifier = 1 + (user.influence * 0.0002)
        else:
            cost_increase_modifier = 1

        total_costs = (round(item['cost'] * cost_increase_modifier)) * quantity
        while total_costs > user.money and quantity > 1:
            quantity = math.floor(user.money / item['cost'])
            total_costs = item['cost'] * quantity

        if total_costs > user.money:
            resp.body = json.dumps({
                'message': 'not_enough_money',
            })
            resp.status = falcon.HTTP_406
            return
        elif total_costs <= user.money:
            user.money -= total_costs
            user.influence += item['influence'] * quantity
            user.guard += item['guard'] * quantity
            user.suspicion_level -= int(round(item['suspicion_level_decrease'] * quantity))
            user.suspicion_level = max(0, user.suspicion_level)
            user.put()

            resp.body = json.dumps({
                'message': 'item_bought',
                'player': user.make_dict(),
                'quantity': quantity
            })
            resp.status = falcon.HTTP_200
            return

        resp.body = json.dumps({
            'message': 'not_enough_money',
        })
        resp.status = falcon.HTTP_406
        return
