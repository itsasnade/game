import falcon
from google.appengine.ext import ndb

from api.models.UserModel import UserModel


class TestResource(object):

    def on_get(self, req, resp):
        resp.status = falcon.HTTP_200  # This is the default status
        resp.body = "API WORKS"
