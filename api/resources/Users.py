import json

import falcon
from google.appengine.api import memcache
from google.appengine.datastore.datastore_query import Cursor

from api.models.UserModel import UserModel


class UsersResource(object):

    def on_get(self, req, resp):
        cursor = req.get_param('cursor')

        memcache_key = "users_" + (cursor or "0")

        return_body = memcache.get(key=memcache_key)
        if return_body:
            resp.body = return_body
            resp.status = falcon.HTTP_200
            return

        if cursor:
            cursor = Cursor(urlsafe=cursor)
        else:
            cursor = Cursor()

        users, next_cursor, more = UserModel.query(
        ).order(-UserModel.average_score).fetch_page(50, start_cursor=cursor)

        return_body = json.dumps({
            'message': 'ok',
            'players': [user.make_dict() for user in users],
            'cursor': next_cursor.urlsafe() and more if next_cursor else None
        })

        memcache.set(memcache_key, return_body, time=600)

        resp.body = return_body
        resp.status = falcon.HTTP_200
