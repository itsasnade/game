import json

import falcon
from google.appengine.ext import ndb


class PlayerResource(object):

    def on_get(self, req, resp):
        user_urlsafe = req.get_param('user_key')
        user_key = ndb.Key(urlsafe=user_urlsafe)
        user = user_key.get()

        if not user:
            resp.body = json.dumps({
                'message': 'user_not_found',
                'player': {}
            })
            resp.status = falcon.HTTP_400
            return

        resp.body = json.dumps({
            'message': 'ok',
            'player': user.make_dict()
        })
        resp.status = falcon.HTTP_200
        return
