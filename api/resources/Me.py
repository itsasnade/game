import json

import falcon

from api.core.helpers.User import get_user_from_request


class MeResource(object):

    def on_get(self, req, resp):
        user = get_user_from_request(req)

        if not user:
            resp.body = json.dumps({
                'message': 'user_not_found',
                'player': {}
            })
            resp.status = falcon.HTTP_400
            return

        resp.body = json.dumps({
            'message': 'ok',
            'player': user.make_dict()
        })
        resp.status = falcon.HTTP_200
        return
