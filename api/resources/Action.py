import falcon
import json
import datetime

from google.appengine.ext import ndb
from random import randint

from api.core.helpers.User import get_user_from_request
from api.models.UserModel import TimerModel
from api.core.static.ActionsList import ACTIONS


class ActionResource(object):

    def on_get(self, req, resp):
        user = get_user_from_request(req)

        if not user:
            resp.body = json.dumps({
                'message': 'user_not_found',
                'player': {}
            })
            resp.status = falcon.HTTP_400
            return

        if user.last_time_actions:
            last_actions = ndb.get_multi(user.last_time_actions or [])
        else:
            last_actions = []

        last_actions_dict = {i.kind: i for i in last_actions}

        for action in ACTIONS:
            kind = action['kind']

            money_bonus_modifier = 1 + (user.influence * 0.005)
            action['reward_money'] = round(action['base_money'] * money_bonus_modifier)

            action['timed_out'] = False
            action['time_passed'] = 0

            if kind in last_actions_dict:
                time_difference = (datetime.datetime.now() - last_actions_dict[kind].last_action).total_seconds()
                if time_difference < action['timeout']:
                    action['timed_out'] = True
                    action['time_passed'] = time_difference

        resp.body = json.dumps({
            "message": "ok",
            "actions": sorted(ACTIONS, key=lambda k: k['order'])
        })
        resp.status = falcon.HTTP_200
        return

    def on_post(self, req, resp):
        kind = str(req.media.get('kind')).lower()

        if not kind:
            resp.body = json.dumps({
                "message": "kind_empty"
            })
            resp.status = falcon.HTTP_400
            return

        user = get_user_from_request(req)

        if not user:
            resp.body = json.dumps({
                'message': 'user_not_found',
                'player': {}
            })
            resp.status = falcon.HTTP_400
            return

        if user.last_time_actions:
            last_actions = ndb.get_multi(user.last_time_actions or [])
        else:
            last_actions = []

        if not last_actions:
            last_actions = []

        current_action_timer = None
        for action in last_actions:
            if action.kind == kind:
                current_action_timer = action
                break

        actions_dict = {item['kind']: item for item in ACTIONS}
        action = actions_dict[kind]

        if not current_action_timer:
            current_action_timer = TimerModel()
            current_action_timer.kind = kind
            current_action_timer.last_action = None
            current_action_timer.put()
            last_actions.append(current_action_timer)

        can_do_action = False
        if current_action_timer.last_action is None:
            can_do_action = True
        else:
            time_difference = (datetime.datetime.now() - current_action_timer.last_action).total_seconds()
            if time_difference > action['timeout']:
                can_do_action = True
            else:
                resp.body = json.dumps({
                    'message': 'action_timeout',
                })
                resp.status = falcon.HTTP_406
                return

        busted = False
        lost_money = 0
        lost_influence = 0
        lost_guard = 0

        if can_do_action:
            current_action_timer.last_action = datetime.datetime.now()
            current_action_timer.put()
            user.last_time_actions = [m.key for m in last_actions]

            rate_higher_then = 100 - action['success_rate']
            rand = randint(0, 100)
            action_status = 'action_fail'

            money_bonus_modifier = 1 + (user.influence * 0.005)
            money = round(action['base_money'] * money_bonus_modifier)

            if rand > rate_higher_then:
                action_status = 'action_success'
                user.money += money
                user.influence += action['base_influence']
                user.guard += action['base_guard']
            else:
                user.suspicion_level += action['suspicion_level_increase']
                if user.suspicion_level >= 100:
                    busted = True

                    lost_money = round(user.money * 0.2)
                    lost_influence = round(user.influence * 0.2)
                    lost_guard = round(user.guard * 0.2)

                    user.money -= lost_money
                    user.influence -= lost_influence
                    user.guard -= lost_guard
                    user.suspicion_level = 0

            money_bonus_modifier = 1 + (user.influence * 0.005)
            next_money = round(action['base_money'] * money_bonus_modifier)

            user.put()

            resp.body = json.dumps({
                'message': action_status,
                'player': user.make_dict(),
                'next_money': next_money,
                'caught': busted,
                'lost_money': lost_money,
                'lost_influence': lost_influence,
                'lost_guard': lost_guard,
            })
            resp.status = falcon.HTTP_200
            return
