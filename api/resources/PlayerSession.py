import falcon
import uuid
import re
import json

from api.core.helpers.User import get_user_from_game_code
from api.models.UserModel import UserModel, GameModel


class PlayerSessionResource(object):

    def on_get(self, req, resp):
        game_code = req.get_param("game_code")
        game_code = re.sub(r"\s+", "", game_code, flags=re.UNICODE)

        if not game_code:
            resp.body = json.dumps({
                "message": "no_game_code_supplied"
            })
            resp.status = falcon.HTTP_400
            return

        player = get_user_from_game_code(game_code)

        if not player:
            resp.body = json.dumps({
                "message": "not_a_valid_game_code"
            })
            resp.status = falcon.HTTP_400
            return

        resp.body = json.dumps({
            "message": "ok",
            "game_code": game_code
        })
        resp.status = falcon.HTTP_200

    def on_post(self, req, resp):
        username = str(req.media.get('username')).strip()
        display_name = username
        username = re.sub(r"\s+", "", username.lower(), flags=re.UNICODE)

        if not username:
            resp.body = json.dumps({
                "message": "username_empty"
            })
            resp.status = falcon.HTTP_400
            return

        query = UserModel.query(
            UserModel.username == username
        )
        count = query.count()
        if count > 0:
            resp.body = json.dumps({
                "message": "username_used"
            })
            resp.status = falcon.HTTP_400
        else:
            game_code = str(uuid.uuid4())

            player = UserModel()
            player.username = username
            player.display_name = display_name
            key = player.put()

            game_model = GameModel(id=game_code)
            game_model.user = key
            game_model.put()

            resp.body = json.dumps({
                "message": "ok",
                "username": player.username,
                "game_code": game_code
            })
            resp.status = falcon.HTTP_201
