import falcon

from api.core.middleware.CorsMiddleware import CorsMiddleware
from api.core.middleware import SerializerMiddleware
from api.core.middleware.AuthMiddleware import AuthMiddleware
from api.resources.Action import ActionResource
from api.resources.Shop import ShopResource
from api.resources.Me import MeResource
from api.resources.User import PlayerResource
from api.resources.Users import UsersResource

from api.resources.test import TestResource
from api.resources.PlayerSession import PlayerSessionResource

app = falcon.API(
    middleware=[
        CorsMiddleware(),
        SerializerMiddleware(),
        AuthMiddleware(),
    ]
)

app.add_route('/api/test', TestResource())
app.add_route('/api/game', PlayerSessionResource())
app.add_route('/api/player', PlayerResource())
app.add_route('/api/me', MeResource())
app.add_route('/api/action', ActionResource())
app.add_route('/api/shop', ShopResource())
app.add_route('/api/users', UsersResource())

