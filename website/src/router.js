import Vue from 'vue';
import Router from 'vue-router';
import Game from './views/Game';
import About from './views/About';
import Actions from './views/game/Actions';
import Shop from './views/game/Shop';
import LeaderBoard from './views/game/LeaderBoard';

Vue.use(Router);

export default new Router({
    mode: 'history',
    base: process.env.BASE_URL,
    linkExactActiveClass: "is-active",
    routes: [
        {
            path: '/',
            redirect: '/game',
        },
        {
            path: '/about',
            name: 'about',
            component: About,
        },
        {
            path: '/game',
            name: 'game',
            redirect: '/game/actions',
            component: Game,
            children: [
                {
                    path: '/game/leaderboard',
                    name: 'game-leaderboard',
                    component: LeaderBoard
                },
                {
                    path: '/game/actions',
                    name: 'game-actions',
                    component: Actions
                },
                {
                    path: '/game/shop',
                    name: 'game-shop',
                    component: Shop
                }
            ]
        },
        {
            path: '/setup',
            name: 'setup',
            component: () => import(/* webpackChunkName: "setup-game" */ './views/setup/Game.vue'),
        },
        {
            path: '/setup/existing',
            name: 'setup-existing',
            component: () => import(/* webpackChunkName: "setup-existing" */ './views/setup/Existing.vue'),
        },
        {
            path: '/setup/new',
            name: 'setup-new',
            component: () => import(/* webpackChunkName: "setup-new" */ './views/setup/New.vue'),
        }
        // {
        //     path: '/about',
        //     name: 'about',
        //     // route level code-splitting
        //     // this generates a separate chunk (about.[hash].js) for this route
        //     // which is lazy-loaded when the route is visited.
        //     component: () => import(/* webpackChunkName: "about" */ './views/About.vue'),
        // },
    ],
});
