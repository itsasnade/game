import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';

import VueResource from 'vue-resource';
import VueCookie from 'vue-cookie';
import Notifications from 'vue-notification';

Vue.config.productionTip = false;

const sessionstorage = require('sessionstorage');

Vue.use(VueResource);
Vue.use(VueCookie);
Vue.use(Notifications);

Vue.http.interceptors.push(request => {
    console.log(request);

    if(sessionstorage.getItem("game_code")){
        request.headers.set('GAME-CODE', sessionstorage.getItem("game_code"));
    }else if(request.url.includes("/api/game") === false){
        router.push('/setup');
        return {};
    }

    return response => {
        if(response.status === 401){
            sessionstorage.removeItem("game_code");
            Vue.cookie.delete("game_code");
            setTimeout(() => {
                router.push('/setup');
            }, 200);
        }
    }
});

const vue_instance = new Vue({
    router,
    store,
    render: h => h(App),
    methods: {
        has_play_session() {
            const self = this;

            let game_code_session = sessionstorage.getItem("game_code");
            let game_code_cookie = self.$cookie.get("game_code");

            if(!game_code_session && !!game_code_cookie){
                game_code_session = game_code_cookie;
                sessionstorage.setItem("game_code", game_code_session);
            }

            if(game_code_session && !game_code_cookie){
                self.$cookie.set("game_code", game_code_session, 31556926);
            }

            return !!game_code_session;
        }
    },
    created() {
        const self = this;
        if (!self.has_play_session() && !self.$route.path.startsWith('/setup')) {
            router.push('/setup');
        }

        if(process.env.NODE_ENV === "development"){
            ROOT_URL = "http://localhost:8080";
        }
    }
}).$mount('#app');

router.beforeEach((to, from, next) => {
    if (to.path.startsWith('/setup')) {
        next();
        return;
    }

    if (vue_instance.has_play_session()) {
        next();
    } else {
        next('/setup');
    }
});