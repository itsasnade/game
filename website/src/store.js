import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        loaded_before: false,
        current_user_old: {
            influence: 0,
            suspicion_level: 0,
        },
        current_user: {
            urlsafe: "",
            display_name: null,
            guard: 0,
            influence: 0,
            mission_success_rate: 0,
            money: 0,
            username: "",
            suspicion_level: 0,
        },
        actions: [],
        shop_items: [],
        busted_info: {
            busted: false,
            lost_money: 0,
            lost_influence: 0,
            lost_guard: 0,
        }
    },
    mutations: {
        set_actions(state, actions) {
            state.actions = actions
        },
        set_shop_items(state, items) {
            state.shop_items = items
        },
        set_player(state, player) {
            state.current_user_old = JSON.parse(JSON.stringify(state.current_user));
            state.current_user = player;

            if(!state.loaded_before){
                state.current_user_old = JSON.parse(JSON.stringify(state.current_user));
                state.loaded_before = true;
            }
        },
        set_busted(state, busted_info = null){
            state.busted_info = busted_info;

            if(!state.busted_info.busted){
                state.busted_info = {
                    busted: false,
                    lost_money: 0,
                    lost_influence: 0,
                    lost_guard: 0,
                }
            }
        }
    },
    actions: {
        get_actions(context) {
            Vue.http.get(ROOT_URL + '/api/action', {}).then(response => {
                const body = response.body;
                const actions = body.actions;
                context.commit("set_actions", actions);
            });
        },
        get_player(context) {
            Vue.http.get(ROOT_URL + '/api/me', {}).then(response => {
                const body = response.body;
                const player = body.player;
                context.commit("set_player", player);
            });
        },
        get_shop_items(context) {
            Vue.http.get(ROOT_URL + '/api/shop', {}).then(response => {
                const body = response.body;
                const items = body.items;
                context.commit("set_shop_items", items);
            });
        }
    },
});
